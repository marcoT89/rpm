angular.module 'page-loader',[
    'ui.router',
    'pascalprecht.translate',
  ]
  # Componente para carregamento de página
  # Uso:
  #   <page-loader></page-loader>
  #   <page-loader delay="1000"></page-loader>
  .directive 'pageLoader', ($state, $timeout) ->
    'ngInject'
    directive =
      replace: true
      restrict: 'E'
      scope:
        delay: '=?'
      templateUrl: 'page-loader/page-loader.html'
      link: (scope, element, attrs) ->
        timer = null
        scope.delay = scope.delay || 100

        # Mostra o loader com um delay
        showLoader = (delay) ->
          return if timer?

          timer = $timeout(() ->
            element.fadeIn()
          , delay)

        # Esconde o loader e cancela o timer
        hideLoader = () ->
          $timeout.cancel(timer)
          timer = null
          element.fadeOut('fast')

        scope.$on '$stateChangeStart', () -> showLoader(scope.delay)
        scope.$on '$stateChangeSuccess', () -> hideLoader()
        scope.$on '$stateChangeError', () ->
          hideLoader()
          $state.go '404'
