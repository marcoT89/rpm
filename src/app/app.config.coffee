angular.module 'app'
  .config ($translateProvider) ->
    'ngInject'
    $translateProvider.preferredLanguage 'pt_BR'
    $translateProvider.useSanitizeValueStrategy null
