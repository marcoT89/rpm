angular.module 'app'
  .config ($stateProvider, $urlRouterProvider, $locationProvider) ->
    'ngInject'
    $stateProvider
      .state 'app',
        abtract: true
        views:
          index:
            templateUrl: 'app/app.html'
      .state 'app.home',
        url: '/home',
        views:
          content:
            templateUrl: 'theme/views/blank.html'
      .state '404',
        url: '/404'
        views:
          'index':
            templateUrl: 'theme/views/404.html'

    $urlRouterProvider.when '', '/home'
    $urlRouterProvider.otherwise '404'

    # $locationProvider.html5Mode(true)
