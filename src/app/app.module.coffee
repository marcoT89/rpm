angular.module 'app', [
  'ui.router',
  'ngSanitize',
  'ngAnimate',
  'html',
  'pascalprecht.translate',
  'i18n',
  'theme',
  'monospaced.elastic',
  # App
  'app.auth',
  'app.api',
  'clients',
  'referrals',
  'wr.users',
  'page-loader',
]
