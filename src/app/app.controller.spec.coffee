describe 'AppController', ->
  beforeEach module 'app'
  it 'should have a title', inject ($controller, $rootScope) ->
    @scope = $rootScope.$new()
    $controller('AppController as vm', $scope: @scope)
    expect(@scope.vm.title).toBe('Project Title')
