routes = ($stateProvider) ->
  'ngInject'

  getClients = (API) ->
    'ngInject'
    API.get('/clients').then (response) -> response.data
  getClient = (API, $stateParams) ->
    'ngInject'
    API.get('/clients/' + $stateParams.id).then (response) -> response.data

  $stateProvider
    .state 'app.clients',
      url: '/clients'
      abstract: true
      views:
        content:
          template: '<div ui-view="content" class="fade-in"></div>'
    .state 'app.clients.list',
      url: ''
      views:
        content:
          template: '<clients-list clients="vm.clients"></clients-list>'
          controller: 'GetClientsController as vm'
          resolve:
            clients: getClients
    .state 'app.clients.new',
      url: '/new'
      views:
        content:
          template: '<client-form></client-form>'
    .state 'app.clients.view',
      url: '/:id'
      views:
        content:
          template: '<client client="vm.client"></client>'
          controller: (client) ->
            @client = client
            return
          controllerAs: 'vm'
          resolve:
            client: getClient
    .state 'app.clients.edit',
      url: '/:id/edit'
      views:
        content:
          template: '<client-form client="vm.client"></client-form>'
          controller: (client) ->
            @client = client
            return
          controllerAs: 'vm'
          resolve:
            client: getClient

angular.module 'app'
  .config routes
