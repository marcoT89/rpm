Routes = ($stateProvider) ->
  'ngInject'
  $stateProvider
    .state 'app.auth',
      url: '/auth'
      abstract: true
    .state 'app.auth.login',
      url: '^/login'
      views:
        'index@':
          templateUrl: 'auth/views/login.html'
          controller: 'AuthLoginController as vm'
    .state 'app.auth.logout',
      url: '^/logout'
      views:
        'index@':
          templateUrl: 'auth/views/login.html'
          controller: 'AuthLogoutController'
    .state 'app.auth.404',
      url: '^/404'
      views:
        'index@':
          templateUrl: 'auth/views/404.html'

angular.module 'app.auth'
  .config Routes

