angular.module 'app'
.config ($stateProvider) ->
  
  getReferrals = (API) ->
    'ngInject'
    API.get('/referrals').then (response) -> response.data

  getReferral = (API, $stateParams) ->
    'ngInject'
    # API.get('/referrals/' + $stateParams.id).then (response) -> response.data
    API.get('/referrals').then (response) ->
      response.data =
        name: 'YEah'

  $stateProvider
    .state 'app.referrals',
      url: '/referrals'
      abstract: true
      views:
        content:
          template: '<div ui-view="content" class="fade-in"></div>'

    .state 'app.referrals.list',
      url: ''
      views:
        content:
          templateUrl: 'referrals/components/referrals-list/referrals.html'
          controller: 'GetReferralsController as vm'
          resolve:
            referrals: getReferrals
    .state 'app.referrals.new',
      url: '/new'
      views:
        content:
          template: 'yeah<referral-form></referral-form>'
    .state 'app.referrals.view',
      url: '/:id'
      views:
        content:
          template: '<referral model="vm.referral"></referral>'
          controller: (referral) ->
            @referral = referral
            return
          controllerAs: 'vm'
          resolve:
            referral: getReferral