angular.module('wr.users')
  .config ($stateProvider) ->
    'ngInject'
    # Get users for UsersController
    getUsers = (userService) ->
      userService.all().then (users) -> return users
    getUsers.$inject = ['userService']

    # Gets user by id for UserFormController
    getUser = (userService, $stateParams) ->
      userService.get($stateParams.id).then (user) -> return user
    getUser.$inject = ['userService', '$stateParams']

    # Creates a new user
    newUser = (userService) -> return userService.newUser()
    newUser.$inject = ['userService']

    $stateProvider
      .state 'app.users',
        abstract: true
        url: '/users'
        views:
          'content':
            template: '<div ui-view="content" class="fade-in"></div>'
      .state 'app.users.list',
        url: ''
        views:
          'content':
            template: '<users-list users="vm.users"></users-list>'
            controller: 'GetUsersController as vm'
            resolve:
              users: getUsers
      .state 'app.users.edit',
        url: '/:id/edit'
        views:
          'content':
            template: '<user-form user="vm.user"></user-form>'
            controller: 'GetUserController as vm'
            resolve:
              user: getUser
      .state 'app.users.new',
        url: '/new'
        views:
          'content':
            template: '<user-form user="vm.user"></user-form>'
            controller: 'GetUserController as vm'
            resolve:
              user: newUser
