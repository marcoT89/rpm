angular.module 'app'
  .config ($stateProvider) ->
    'ngInject'

    getOrder = (orderService, $stateParams) ->
      'ngInject'
      orderService.getOrder($stateParams.id).then (order) ->
        return order

    $stateProvider

      .state 'app.orders',
        url: '/orders'
        abstract: true
        views:
          content:
            template: '<div ui-view="content" class="fade-in"></div>'

      .state 'app.orders.list',
        url: ''
        views:
          content:
            controller: (orders) ->
              'ngInject'
              @orders = orders
              return
            controllerAs: 'vm'
            template: '<orders-table orders="vm.orders"></orders-table>'
            resolve:
              orders: (orderService) ->
                'ngInject'
                return orderService.all()

      .state 'app.orders.view',
        url: '/:id'
        views:
          content:
            template: '<order order="vm.order"></order>'
            controller: (order) ->
              @order = order
              console.log 'order', order
              return
            controllerAs: 'vm'
            resolve:
              order: getOrder