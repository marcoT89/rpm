angular.module 'wr.users'
  .component 'userForm',
    templateUrl: 'users/components/user-form/user-form.html'
    controller: 'UserFormController as vm'
    bindings:
      user: '=?'