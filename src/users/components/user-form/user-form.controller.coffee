# UserFormController.$inject = ['userService', 'user', '$state']
angular.module 'wr.users'
  .controller 'UserFormController', (userService, $state, $window) ->
    'ngInject'

    init = () =>
      @user = {} unless @user?
      @submitted = false
      @submit = submit
      @back = () -> $window.history.back()

    ###############

    submit = (form) =>
      @submitted = true
      return if form.$invalid
      userService.save(@user).then (user) ->
        $state.go 'app.users.list'
        return user

    init()

    return
