angular.module 'wr.users'
  .component 'usersList',
    templateUrl: 'users/components/users-list/users-list.html'
    controller: 'UsersListController as vm'
    bindings:
      users: '<'