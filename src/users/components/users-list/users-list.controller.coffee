angular.module 'wr.users'
  .controller 'UsersListController', (userService, notify) ->
    'ngInject'

    # Initialize controller
    init = () ->
      getUsers() unless @users?

    # Requests users from api
    getUsers = () ->
      @loading = true
      userService.all().then (users) -> setUsers(users)

    # Sets users correctly
    setUsers = (users) ->
      @loading = false
      @users = users
      @displayedUsers = [].concat @users

    # Destroys a user
    @destroy = (user) ->
      userService.delete(user).then () => @users.splice(@users.indexOf(user), 1)

    init()
    return