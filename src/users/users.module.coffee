angular.module 'wr.users', [
  'pascalprecht.translate',
  'smart-table',
  'ngMessages',
  'ui.router',
  'ui.bootstrap',
  'angular-confirm',
  # Project Dependencies
  'app.api',
  'wr.password-toggle',
  'i18n',
  'html',
  'wr.notify',
]
