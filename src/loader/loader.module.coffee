angular.module 'loader', []
  .component 'loader',
    templateUrl: 'loader/loader.html'
    controllerAs: 'vm'
    bindings:
      class: '@?'
