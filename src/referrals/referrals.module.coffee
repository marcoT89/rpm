angular.module 'referrals', [
  'ui.bootstrap',
  'ui.router',
  'localytics.directives',
  'ui.utils.masks',
  'smart-table',
  'idf.br-filters',
  'ngAnimate',
  # app dependencies
  'app.api',
]
