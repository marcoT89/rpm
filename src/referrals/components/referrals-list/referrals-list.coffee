angular.module 'referrals'
  .component 'referralsList',
    templateUrl: 'referrals/components/referrals-list/referrals-list.html'
    controller: 'ReferralsListController as vm'
    bindings:
      referrals: '<?'