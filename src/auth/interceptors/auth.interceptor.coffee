Interceptor = ($httpProvider) ->
  'ngInject'
  $httpProvider.interceptors.push AuthInterceptor

Interceptor.$inject = ['$httpProvider']

AuthInterceptor = ($q, $injector) ->
  'ngInject'
  return {
    request: (config) ->
      token = $injector.get('$localStorage').token
      if token
        config.headers.Authorization = 'Bearer ' + token
      return config
    responseError: (rejection) ->
      if rejection.status == 401
        $injector.get('$state').go('app.auth.login')
        return $q.reject(rejection)
      # if rejection.status == 404
      #   $injector.get('$state').go('app.auth.404')
      #   return $q.reject(rejection)
      return rejection
  }

AuthInterceptor.$inject = ['$q', '$injector']

angular.module 'app.auth'
  .config Interceptor
