angular.module 'app.auth'
  .controller 'AuthLoginController', ($state, AuthService, notify) ->
    'ngInject'
    @credentials = {}
    @submit = (form) =>
      return if form.$invalid
      AuthService.authenticate(@credentials).then (response) -> $state.go('app.users.list')
        .catch (error) -> notify.error message: 'auth.login.failed'
    return
