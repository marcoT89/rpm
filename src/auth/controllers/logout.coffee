angular.module 'app.auth'
  .controller 'AuthLogoutController', (AuthService) ->
    'ngInject'
    AuthService.logout()
