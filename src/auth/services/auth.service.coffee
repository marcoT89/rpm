class AuthService
  constructor: ($state, API, $localStorage, $rootScope) ->
    @state = $state
    @api = API
    @storage = $localStorage
    @root = $rootScope

  authenticate: (credentials) =>
    return @api.post('/user_token', auth: credentials).then (response) =>
      @storage.token = response.data.jwt
      return @current_user()

  current_user: =>
    return @api.post('/me').then (response) =>
      @storage.current_user = response.data
      @root.$broadcast('UserAuthenticated', response.data)
      return @storage.current_user
  logout: ->
    delete @storage.current_user
    delete @storage.token
    @state.go('app.auth.login')


AuthService.$inject = ['$state', 'API', '$localStorage', '$rootScope']

angular.module 'app.auth'
  .service 'AuthService', AuthService
