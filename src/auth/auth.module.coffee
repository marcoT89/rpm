angular.module 'app.auth', [
  'pascalprecht.translate',
  'ui.router',
  'ui.bootstrap',
  'ngStorage',
  # Project Dependencies
  'app.api',
  'wr.notify',
  'i18n',
  'html',
  # module components
  # 'app.auth.login-dropdown',
]
