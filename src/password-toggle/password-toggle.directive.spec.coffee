fdescribe 'wr-password-toggle directive', ->
  beforeEach module 'wr.password-toggle'

  scope = element = formGroup = undefined

  html = '<form name="form">
    <div class="form-group">
      <input type="password" name="password" wr-password-toggle>
    </div>
  </form>'

  # getElement = ($compile, $scope) ->
  #   element = $compile(angular.element(html))($scope)
  #   $(element)

  beforeEach inject ($compile, $rootScope) ->
    scope = $rootScope.$new()
    compiled = $compile(angular.element(html))(scope)
    element = $(compiled)

  # it 'should add a span with an icon to form-group element', inject ($compile, $rootScope)->
  #   # Arrange
  #   formGroup = element.find('.form-group')
  #   # Act e Assert
  #   expect(formGroup.has('span.wr-input-icon')).toBeTruthy()

  # fit 'should change input type to text if it is password and icon classes on click', ->
  #   # Arrange
  #   formGroup = element.find('.form-group')
  #   input = formGroup.find('input')
  #   console.log 'input', input
  #   expect(input.attr('type')).toEqual('password')
  #   expect(formGroup.find('.wr-input-icon i.fa').hasClass('fa-eye')).toBeTruthy()
  #   expect(formGroup.find('.wr-input-icon i.fa').hasClass('fa-eye-slash')).toBeFalsy()
  #   # Act
  #   formGroup.find('.wr-input-icon').triggerHandler('click')
  #   # Assert
  #   expect(input.attr('type')).toEqual('text')
  #   expect(formGroup.find('.wr-input-icon i.fa').hasClass('fa-eye')).toBeFalsy()
  #   expect(formGroup.find('.wr-input-icon i.fa').hasClass('fa-eye-slash')).toBeTruthy()

  # it 'should change input type to password if it is text and icon classes on click', ->
  #   # Arrange
  #   expect(formGroup.find('input').attr('type')).toEqual('text')
  #   expect(formGroup.find('.wr-input-icon i.fa').hasClass('fa-eye')).toBeFalsy()
  #   expect(formGroup.find('.wr-input-icon i.fa').hasClass('fa-eye-slash')).toBeTruthy()
  #   # Act
  #   $(formGroup.find('.wr-input-icon')).click()
  #   # Assert
  #   expect(formGroup.find('input').attr('type')).toEqual('password')
  #   expect(formGroup.find('.wr-input-icon i.fa').hasClass('fa-eye')).toBeTruthy()
  #   expect(formGroup.find('.wr-input-icon i.fa').hasClass('fa-eye-slash')).toBeFalsy()
