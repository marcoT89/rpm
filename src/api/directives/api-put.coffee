apiPut = (API, ApiDirectives) ->
  'ngInject'
  directive =
    restrict: 'A'
    link: (scope, element, attrs) ->
      ApiDirectives.configure
        directiveName: 'apiPut'
        method: 'put'
        scope: scope
        element: element
        attrs: attrs

angular.module 'app.api'
  .directive 'apiPut', apiPut
