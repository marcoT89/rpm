apiDelete = (API, ApiDirectives) ->
  'ngInject'
  directive =
    restrict: 'A'
    link: (scope, element, attrs) ->
      url = attrs.apiUrl || element.closest('[api-url]').attr('api-url')
      throw new Error('api-url attribute required.') unless url?
      # object
      object = scope.$eval attrs['apiDelete']
      # list = scope.$eval attrs['apiFromList']
      # events
      event = attrs.onEvent || 'click'
      # on success action
      onSuccess = scope.$eval attrs.onSuccess
      onError = scope.$eval attrs.onError
      onDelete = scope.$eval attrs.onDelete

      # perform action
      element.on event, () ->
        # deleted_object = list.splice(list.indexOf(object), 1)[0] if list?
        API['delete'](url).then((response) ->
          onSuccess(response, object) if onSuccess?
        , onError)

angular.module 'app.api'
  .directive 'apiDelete', apiDelete
