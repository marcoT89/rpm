apiGet = (API, ApiDirectives) ->
  'ngInject'
  directive =
    restrict: 'A'
    link: (scope, element, attrs) ->
      ApiDirectives.configure
        directiveName: 'apiGet'
        method: 'get'
        scope: scope
        element: element
        attrs: attrs

angular.module 'app.api'
  .directive 'apiGet', apiGet
