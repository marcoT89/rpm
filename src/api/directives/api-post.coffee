apiPost = (API, ApiDirectives) ->
  'ngInject'
  directive =
    restrict: 'A'
    link: (scope, element, attrs) ->
      ApiDirectives.configure
        directiveName: 'apiPost'
        method: 'post'
        scope: scope
        element: element
        attrs: attrs

angular.module 'app.api'
  .directive 'apiPost', apiPost
