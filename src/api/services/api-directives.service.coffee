class ApiDirectives
  constructor: (API) ->
    @api = API
  configure: (config) ->
    $this = @
    # url
    url = config.attrs.apiUrl || config.element.closest('[api-url]').attr('api-url')
    throw new Error('api-url attribute required.') unless url?
    # object
    object = config.scope.$eval config.attrs[config.directiveName]
    # events
    event = config.attrs.onEvent || 'click'
    # on success action
    onSuccess = config.scope.$eval config.attrs.onSuccess
    onError = config.scope.$eval config.attrs.onError

    # perform action
    config.element.on event, () ->
      $this.api[config.method](url, object).then(onSuccess, onError)

ApiDirectives.$inject = ['API']

angular.module 'app.api'
  .service 'ApiDirectives', ApiDirectives
