describe 'API Service', () ->
  beforeEach module 'app.api'

  describe 'get(url, params):', () ->
    it 'should call $http.get', inject (API, $http, APIConfig) ->
      # Arrange
      spyOn($http, 'get')
      # Act
      API.get '/dummy'
      # Assert
      expect($http.get).toHaveBeenCalledWith(APIConfig.baseUrl + '/api/dummy', undefined)

  describe 'post(url, params):', () ->
    it 'should call $http.post', inject (API, $http, APIConfig) ->
      # Arrange
      spyOn($http, 'post')
      # Act
      API.post('/dummy')
      # Assert
      expect($http.post).toHaveBeenCalledWith(APIConfig.baseUrl + '/api/dummy', undefined)

  describe 'put(url, params):', () ->
    it 'should call $http.put', inject (API, $http, APIConfig) ->
      # Arrange
      spyOn($http, 'put')
      # Act
      API.put('/dummy')
      # Assert
      expect($http.put).toHaveBeenCalledWith(APIConfig.baseUrl + '/api/dummy', undefined)

  describe 'delete(url, params):', () ->
    it 'should call $http.delete', inject (API, $http, APIConfig) ->
      # Arrange
      spyOn($http, 'delete')
      # Act
      API.delete('/dummy')
      # Assert
      expect($http.delete).toHaveBeenCalledWith(APIConfig.baseUrl + '/api/dummy', undefined)
