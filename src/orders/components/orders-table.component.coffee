angular.module 'orders'
  .component 'ordersTable',
    templateUrl: 'orders/views/orders-table.html'
    controller: 'OrdersController as vm'
    bindings:
      orders: '=?'
