angular.module 'orders'
  .component 'order',
    templateUrl: 'orders/views/order.html'
    bindings:
      order: '='