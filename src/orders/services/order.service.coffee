class OrderService
  @$inject: ['API']
  constructor: (@API) ->
  all: () => @API.get('/orders').then (response) -> response.data
  getOrder: (id) => @API.get('/orders/' + id).then (response) -> response.data

angular.module 'orders'
  .service 'orderService', OrderService