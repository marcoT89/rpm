class ClientService
  @$inject: ['API']
  constructor: (@API) ->
  save: (client) =>
    if client.id? then @update(client) else @create(client)
  create: (client) =>
    @API.post '/clients', client: client
  update: (client) =>
    @API.put "/clients/#{client.id}", client: client
  getAll: => @API.get('/clients').then (response) -> return response.data


angular.module 'clients'
  .service 'ClientService', ClientService
