angular.module 'clients'
  .component 'clientForm',
    templateUrl: 'clients/components/client-form/client-form.html'
    controller: 'ClientFormController as vm'
    bindings:
      client: '=?'
      onSuccess: '&?'
      onError: '&?'
