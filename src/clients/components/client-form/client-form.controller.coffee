ClientFormController = ($window, ClientService, notify, $state) ->
  'ngInject'
  init = () =>
    @client = {} unless @client?
    @back = back
    @save = save
    @states = ['DF', 'BA', 'SP', 'RJ']
    @cities = [
      "Águas Claras",
      "Asa Sul",
      "Asa Norte",
      "Arniqueiras"
    ]

  back = () -> $window.history.back()

  save = (client) =>
    ClientService.save(client).then (response) =>
      @client = response.data
      notify.success message: 'clients.action.successfully_saved'
      $state.go 'app.clients.list'

  init()
  return

angular.module 'clients'
  .controller 'ClientFormController', ClientFormController
