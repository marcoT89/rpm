angular.module 'clients'
  .component 'client',
    templateUrl: 'clients/components/client/client.html'
    controllerAs: 'vm'
    bindings:
      client: '='
