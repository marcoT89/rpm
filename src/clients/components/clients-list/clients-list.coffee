angular.module 'clients'
  .component 'clientsList',
    templateUrl: 'clients/components/clients-list/clients-list.html'
    controller: 'ClientsListController as vm'
    bindings:
      clients: '<?'
