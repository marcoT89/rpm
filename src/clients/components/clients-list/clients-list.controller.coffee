ClientsListController = (ClientService) ->
  'ngInject'
  init = () =>
    @getClients = getClients
    @removed = removed
    @getClients() unless @clients?
    return

  removed = (response, removed_client) => @clients.splice(@clients.indexOf(removed_client), 1)

  getClients = () =>
    @loading = true
    ClientService.getAll().then (clients) ->
      setClients(clients)

  setClients = (clients) =>
    @clients = clients
    @displayedClients = [].concat @clients
    @loading = false

  init()

  return

angular.module 'clients'
  .controller 'ClientsListController', ClientsListController
