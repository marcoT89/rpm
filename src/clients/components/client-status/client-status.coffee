angular.module 'clients'
  .component 'clientStatus',
    templateUrl: 'clients/components/client-status/client-status.html'
    controllerAs: 'vm'
    bindings:
      status: '='