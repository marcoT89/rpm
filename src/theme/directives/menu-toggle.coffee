maMenuToggle = () ->
  'ngInject'
  directive =
    restrict: 'A'
    link: (scope, element, attrs) ->
      element.on 'click', (event) ->
        parent = element.parent()
        parent.toggleClass 'toggled'
        parent.find('ul').slideToggle('fast')

angular.module('theme').directive 'maMenuToggle', maMenuToggle
