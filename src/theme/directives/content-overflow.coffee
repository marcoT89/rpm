cOverflow = () ->
  directive =
    restrict: 'C'
    link: (scope, element, attrs) ->
      unless $('html').hasClass('ismobile')
        # scrollService.malihuScroll(element, 'minimal-dark', 'y');
        $(element).mCustomScrollbar
          theme: 'minimal-dark',
          scrollInertia: 100,
          axis:'yx',
          mouseWheel:
            enable: true,
            axis: 'y',
            preventDefault: true

angular.module 'theme'
  .directive 'cOverflow', cOverflow
