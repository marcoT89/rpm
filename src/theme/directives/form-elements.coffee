angular.module 'theme'
  .directive 'fgLine', () ->
    directive =
      restrict: 'C'
      link: (scope, element, attrs) ->
        if $('.fg-line')[0]?
          $('body').on 'focus', '.form-control', ->
            $(this).closest('.fg-line').addClass 'fg-toggled'
          $('body').on 'blur', '.form-control', ->
            group = $(this).closest('.form-group')
            input_value = group.find('.form-control').val()
            if group.hasClass('fg-float')
              if input_value.length == 0
                $(this).closest('.fg-line').removeClass 'fg-toggled'
            else
              $(this).closest('.fg-line').removeClass 'fg-toggled'
            return

  .directive 'btn', () ->
    directive =
      restrict: 'C'
      link: (scope, element, attrs) ->
        if element.hasClass('btn-icon') or element.hasClass('btn-float')
          Waves.attach element, [ 'waves-circle' ]
        else if element.hasClass('btn-light')
          Waves.attach element, [ 'waves-light' ]
        else
          Waves.attach element
        Waves.init()
