sidebarToggle = () ->
  directive =
    restrict: 'A'
    link: (scope, element, attrs) ->
      element.on 'click', (event) ->
        event.stopPropagation()
        $('#sidebar').toggleClass 'toggled'
        element.toggleClass 'toggled'

angular.module 'theme'
  .directive 'sidebarToggle', sidebarToggle
