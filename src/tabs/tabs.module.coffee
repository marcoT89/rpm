angular.module 'tabs', []
  .directive 'tabToggle', ($document) ->
    'ngInject'
    directive =
      restrict: 'A'
      scope:
        tab: '@tabToggle'
      link: (scope, element) ->
        element.on 'click', ($event) ->
          $event.stopPropagation()
          # remove active class from tab links
          element.closest('li').siblings().removeClass('active')
          # add active class to tab element
          element.closest('li').addClass('active')

          tabContent = $($document).find('#' + scope.tab)
          # remove active class from tab contents
          tabContent.siblings().removeClass('active')
          # activate tab content
          tabContent.addClass('active')
