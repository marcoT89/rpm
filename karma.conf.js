// Karma configuration
// Generated on Tue Jul 19 2016 20:36:39 GMT-0300 (ART)
module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['phantomjs-shim', 'jasmine', 'angular-filesort'],


    // list of files / patterns to load in the browser
    files: [/* gerado no gulp task */],


    // list of files to exclude
    exclude: [
        'tmp/js/theme.js',
        'tmp/js/vendor.js',
    ],

    angularFilesort: {
        whitelist: ['tmp/**/!(*.html|*.spec|*.mock).js']
    },

    plugins : [
      'karma-phantomjs-launcher',
      'karma-angular-filesort',
      'karma-phantomjs-shim',
      'karma-coverage',
      'karma-jasmine',
      'karma-mocha-reporter',
      'karma-ng-html2js-preprocessor'
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        'tmp/js/app/**/!(*.spec|*.mock).js': ['coverage'],
        'tmp/js/theme/**/!(*.spec|*.mock).js': ['coverage'],
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['coverage', 'mocha'],

    coverageReporter: {
        reporters: [
            {type: 'html', dir: 'reports/coverage/'},
            {type: 'text'}
        ],
    },


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,
    autoWatchBatchDelay: 500,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
