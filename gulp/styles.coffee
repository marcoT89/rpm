gulp    = require 'gulp'
g       = require('gulp-load-plugins')()
paths   = require('./config').paths
vendor  = require('./config').vendor
wiredep = require('wiredep')

sass = ->
  gulp.src './src/**/*.scss'
    .pipe g.sass()
    .pipe g.concat 'app.css'
    .pipe gulp.dest paths.tmp_css

css_vendor = ->
  cssFilter = g.filter('**/*.css');
  gulp.src vendor.styles.concat wiredep().css
    .pipe cssFilter
    # .pipe g.print()
    .pipe g.concat 'vendor.css'
    .pipe gulp.dest paths.tmp_css

gulp.task 'sass', sass
gulp.task 'css:vendor', css_vendor
gulp.task 'styles', ['sass', 'css:vendor']
