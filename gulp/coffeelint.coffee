gulp = require 'gulp'
stylish = require 'coffeelint-stylish'
g = require('gulp-load-plugins')()

gulp.task 'coffeelint', ->
  return gulp.src(['./src/**/*.coffee'])
    .pipe(g.coffeelint())
    .pipe(g.coffeelint.reporter(stylish))
    .pipe(g.coffeelint.reporter('fail'))
