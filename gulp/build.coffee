gulp     = require 'gulp'
g        = require('gulp-load-plugins')()
paths    = require('./config').paths
config   = require './config'
bower    = require 'main-bower-files'
notifier = require 'node-notifier'
wiredep  = require 'wiredep'

build = ->
  gulp.src [
    paths.tmp_css + '/vendor.css',
    paths.tmp_css + '/theme.css',
    paths.tmp_css + '/*.css',
  ]
    .pipe g.concat('app.min.css')
    .pipe g.cssmin()
    .pipe gulp.dest paths.dist
    .pipe g.size(showFiles: true)

  gulp.src [
    paths.tmp_js + '/vendor.js',
    paths.tmp_js + '/theme.js',
    paths.tmp_js + '/**/*.module.js'
    paths.tmp_js + '/**/!(*.spec).js'
  ]
    .pipe g.concat('app.min.js')
    .pipe g.ngAnnotate()
    .pipe g.uglify()
    .pipe gulp.dest paths.dist
    .pipe g.size(showFiles: true)

# Tasks
# gulp.task 'build', ['coffeelint', 'sass', 'html', 'i18n', 'theme']
gulp.task 'build', ['scripts', 'styles'], build

