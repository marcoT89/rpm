gulp    = require 'gulp'
paths   = require('./config').paths
browser = require('browser-sync')
g       = require('gulp-load-plugins')()

watch_files = ->
  gulp.watch('src/**/*', ['reload'])
  gulp.watch 'bower.json', ['reload']

# Tasks
gulp.task 'watch', watch_files
gulp.task 'reload', ['inject'], browser.reload
