gulp    = require 'gulp'
g       = require('gulp-load-plugins')()
paths   = require('./config').paths
vendor  = require('./config').vendor
wiredep = require 'wiredep'

compile_scripts = ->
  gulp.src paths.src + '/**/*.coffee'
    .pipe g.coffee()
    .pipe gulp.dest paths.tmp_js

scripts_vendor = ->
  gulp.src vendor.scripts.concat wiredep().js
    # .pipe g.print()
    .pipe g.concat('vendor.js')
    .pipe gulp.dest paths.tmp_js
    .pipe g.size showFiles: true

gulp.task 'compile-scripts', compile_scripts
gulp.task 'scripts', ['coffeelint', 'html', 'i18n', 'theme', 'scripts:vendor', 'compile-scripts']
gulp.task 'scripts:vendor', scripts_vendor
