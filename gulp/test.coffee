gulp    = require 'gulp'
karma   = require 'karma'
wiredep = require 'wiredep'
paths   = require('./config').paths

# Tipo de reporters para o modo Watch
watch_mode_coverage_reporters = [
  {type: 'html', dir: 'reports/coverage/'},
  {type: 'text-summary'},
]
watch_mode_reporters = ['coverage', 'progress']

getFiles = ->
  files = wiredep(devDependencies: true).js.concat [
    'tmp/**/*.module.js',
    'tmp/**/*.js'
  ]
  return files

run_tests = (test) ->
  localConfig =
    files: getFiles()
    singleRun: !!test.single_run
    autoWatch: !test.single_run
    configFile: __dirname + '/../karma.conf.js'

  if ! test.single_run
    console.log 'não single'
    localConfig.coverageReporter = reporters: watch_mode_coverage_reporters
    localConfig.reporters = watch_mode_reporters
  else
    console.log 'é single'

  server = new karma.Server(localConfig)
  server.start()

single_test = ->
  run_tests single_run: true

watch_test = ->
  gulp.watch(paths.src + '/**/*', ['reload'])
  run_tests single_run: false

gulp.task 'test', ['inject'], single_test
gulp.task 'test:auto', ['inject'], watch_test

