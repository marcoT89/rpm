gulp  = require 'gulp'
g     = require('gulp-load-plugins')()
paths = require('./config').paths

i18n = ->
  gulp.src [paths.src + '/**/lang/*.json']
    .pipe g.angularTranslate('i18n.module.js', module: 'i18n')
    .pipe gulp.dest paths.tmp_js

gulp.task 'i18n', i18n
