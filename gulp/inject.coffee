gulp     = require 'gulp'
g        = require('gulp-load-plugins')()
paths    = require('./config').paths
messages = require('./config').messages
bower    = require 'main-bower-files'
es       = require 'event-stream'

inject = ->
  js_sources = gulp.src([
    paths.tmp_js + '/vendor.js',
    paths.tmp_js + '/theme.js',
    paths.tmp_js + '/**/*.module.js'
    paths.tmp_js + '/**/!(*.spec).js'
  ], read: false)

  css_sources = gulp.src([
    paths.tmp + '/**/vendor.css',
    paths.tmp + '/**/theme.css',
    paths.tmp + '/**/*.css'
  ], read: false)

  gulp.src paths.src + '/index.html'
    .pipe g.inject(js_sources, {ignorePath: paths.tmp})
    .pipe g.inject(css_sources, {ignorePath: paths.tmp})
    .pipe gulp.dest paths.tmp
    .pipe g.notify(messages.build_success)

inject_dist = ->
  js_sources = gulp.src(paths.dist + '/*.js', read: false)
  css_sources = gulp.src(paths.dist + '/*.css', read: false)
  gulp.src paths.src + '/index.html'
    .pipe g.inject(css_sources, {ignorePath: paths.dist})
    .pipe g.inject(js_sources, {ignorePath: paths.dist})
    .pipe gulp.dest paths.dist
    .pipe g.notify(messages.build_dist_success)

gulp.task 'inject', ['scripts', 'styles'], inject
gulp.task 'inject:dist', ['build'], inject_dist
