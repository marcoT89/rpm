gulp  = require 'gulp'
theme = require('./config').theme
paths = require('./config').paths
g     = require('gulp-load-plugins')()

theme_js = ->
  gulp.src theme.scripts
    .pipe g.concat 'theme.js'
    .pipe gulp.dest paths.tmp_js
    .pipe g.size showFiles: true

theme_css = ->
  gulp.src theme.styles
    .pipe g.concat 'theme.css'
    .pipe gulp.dest paths.tmp_css
    .pipe g.size showFiles: true

theme_files = ->
  gulp.src theme.images
    .pipe gulp.dest paths.tmp + '/img'
  gulp.src theme.fonts
    .pipe gulp.dest paths.tmp + '/fonts'

gulp.task 'theme:js', theme_js
gulp.task 'theme:css', theme_css
gulp.task 'theme:others', theme_files

gulp.task 'theme', ['theme:js', 'theme:css', 'theme:others']
