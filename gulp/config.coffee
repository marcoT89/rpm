module.exports =
  paths:
    src: 'src'
    dist: 'dist'
    reports: 'reports'
    tmp: 'tmp'
    tmp_js: 'tmp/js'
    tmp_css: 'tmp/css'
    tmp_test: 'tmp-tests'
    index: 'src/index.html'
  messages:
    build_success: 'Build realizada com sucesso'
    build_dist_success: 'Build dist realizada com sucesso'
  wiredep:
    test_options:
      devDependencies: true
  vendor:
    scripts: [
      'bower_components/jquery/dist/jquery.js',
      'bower_components/bootstrap/dist/js/bootstrap.min.js',
      # theme vendor
    ]
    styles: [
      'bower_components/bootstrap/dist/css/bootstrap.min.css',
      'bower_components/animate.css/animate.css',
    ]
  theme:
    images: 'theme/img/**/*'
    fonts: ['theme/fonts/**/*', 'bower_components/material-design-iconic-font/dist/fonts/**/*']
    styles: [
      'theme/css/app_1.min.css',
      'theme/css/app_2.min.css',
    ]
    scripts: [
      'theme/scripts/ui/accordion.js',
      'theme/scripts/ui/animate.js',
      'theme/scripts/ui/link-transition.js',
      'theme/scripts/ui/panel-controls.js',
      'theme/scripts/ui/preloader.js',
      'theme/scripts/ui/toggle.js',
      'theme/scripts/urban-constants.js',
      'theme/scripts/extentions/lib.js',
    ]
