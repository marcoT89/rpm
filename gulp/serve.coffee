gulp    = require 'gulp'
browser = require('browser-sync')
paths = require('./config').paths
g       = require('gulp-load-plugins')()

serve = ->
  init_browser_in paths.tmp

serve_dist = ->
  gulp.start 'copy-files'
  init_browser_in paths.dist

init_browser_in = (path) ->
  browser.init server:
    baseDir: path

copy_files = ->
  gulp.src paths.tmp + '/images/**/*'
    .pipe gulp.dest paths.dist + '/images'
  gulp.src paths.tmp + '/fonts/**/*'
    .pipe gulp.dest paths.dist + '/fonts'

clean = ->
  gulp.src [paths.tmp, paths.dist, paths.tmp_test, paths.reports]
    .pipe g.clean()

# Server tasks
gulp.task 'serve', ['inject', 'test:auto'], serve
gulp.task 'serve:dist', ['inject:dist'], serve_dist
gulp.task 'copy-files', copy_files
gulp.task 'clean', clean
