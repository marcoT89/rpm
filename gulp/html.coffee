gulp  = require 'gulp'
g     = require('gulp-load-plugins')()
paths = require('./config').paths

html = ->
  gulp.src [paths.src + '/**/*.html', '!' + paths.index]
    .pipe g.angularTemplatecache(standalone: true, filename: 'html.module.js', module: 'html')
    .pipe gulp.dest paths.tmp_js

gulp.task 'html', html
